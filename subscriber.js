const Reader = require('./lib/nsq-client/reader')
const {
  NSQLOOKUP_HOST,
  NSQ_TOPIC,
  NSQ_CHANNEL
} = process.env

const reader = new Reader(NSQLOOKUP_HOST, NSQ_TOPIC, NSQ_CHANNEL)

reader.reader.on('message', message => {
  console.log(message.body)
  message.finish()
})
