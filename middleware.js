const {
  NSQD_HOST,
  NSQD_PORT
} = process.env
const Writer = require('./lib/nsq-client/writer')
const writer = new Writer(NSQD_HOST, NSQD_PORT)

module.exports = (req, res, next) => {
  const {
    url,
    header,
    params,
    body
  } = req
  const data = { url, header, params, body }

  writer.writer.publish(process.env.NSQ_TOPIC, data)
  next()
}
