# Usage

## Middleware

For express.js

```
const express = require('express')
const middleware = require('./middleware')

const app = express()

// before any route
app.use(middleware)

app.get('/', (req, res) => {
  res.json({ message: 'OK' })
})

app.listen('3000', () => {
  console.log('server listening on port 3000')
})
```

## subscriber

```
$ node subscriber.js
```
