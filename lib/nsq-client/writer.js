const nsq = require('nsqjs');

function Writer (host, port) {
  this.host = host
  this.port = port

  const writer = new nsq.Writer(host, port)

  writer.connect()

  writer.on('ready', () => {
    console.log('NSQ Writer connected')
  })

  writer.on('closed', () => {
    console.log('NSQ Writer closed')
  })

  this.writer = writer
}

module.exports = Writer
