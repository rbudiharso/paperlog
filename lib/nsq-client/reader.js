const nsq = require('nsqjs')

function Reader (host, topic, channel) {
  this.host = host
  this.topic = topic
  this.channel = channel

  const reader = new nsq.Reader(topic,channel, {
    lookupdHTTPAddresses: host
  })

  reader.connect()

  this.reader = reader
}

module.exports = Reader
